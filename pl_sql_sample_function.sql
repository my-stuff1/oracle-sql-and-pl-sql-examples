/*
example of pl/sql function, written by myself - based on real function
*/



CREATE OR REPLACE FUNCTION 
--function that assembles the request, that will be further passed to another function, that creates and html-code for result table of passed request

REQUEST_FOR_HTML_CREATE

  (
		PROCESSED_TABLE_NAME IN CLOB DEFAULT null
  )

	RETURN CLOB

IS


monitored_column_names SOME_CLOB_COLLECTION;
get_error_amount_req_body CLOB;
error_rate NUMBER;
selected_column_names SOME_CLOB_COLLECTION;

final_request_columns_names_for_select_for_html CLOB;
column_names_1st_2 SOME_CLOB_COLLECTION;

selected_column_names_count NUMBER;

final_request_for_html CLOB;



BEGIN
	
--1) create column list according to given criteria

---create 1st list of columns
select 
		COLUMN_NAME bulk collect
	INTO
		monitored_column_names
	from all_tab_columns 
	where 1=1
	and lower(data_type) like '%number%' 
	and lower(table_name) = to_char(lower(PROCESSED_TABLE_NAME))
	and lower(column_name) not like '%total_cnt%'	
	and lower(column_name) not like '%$css%'
;

---create 2nd list of columns based on the 1st list, based on some criteria 
	selected_column_names := SOME_CLOB_COLLECTION();

  FOR K IN (
    select COLUMN_VALUE AS COLUMN_NAME from TABLE(monitored_column_names)    
    )
  LOOP
    

      
      get_error_amount_req_body := '
       SELECT 
        SUM( CASE WHEN ( ' || K.COLUMN_NAME || '  / TOTAL_CNT  * 100) >= 1 THEN 1 ELSE 0 END) FROM ' || PROCESSED_TABLE_NAME
      ;
      

			
				EXECUTE IMMEDIATE get_error_amount_req_body
				INTO error_rate;

	

			IF error_rate >= 1 THEN 
				BEGIN
				
					selected_column_names.EXTEND;

					selected_column_names(selected_column_names.LAST) := K.COLUMN_NAME;
				
				END;
			END IF;	

	END LOOP;



--2) fill in specific columns of the input table
  FOR K IN (
    select COLUMN_VALUE AS COLUMN_NAME from TABLE(monitored_column_names)    
    )
  LOOP
		EXECUTE IMMEDIATE 'UPDATE ' 
		|| PROCESSED_TABLE_NAME
		|| ' SET '
		|| K.COLUMN_NAME||'$CSS'
		||' = ''bgcolor = "ff0000"'' 
		WHERE 1=1 '		
		
		|| ' AND ' 
		|| K.COLUMN_NAME || ' / TOTAL_CNT * 100 >= 1';
		COMMIT;
	END LOOP;



--3) creating final request
		---start with SELECT

		final_request_columns_names_for_select_for_html := 'SELECT'
		;

		---adding first several columns
						select 
							COLUMN_NAME bulk collect
						INTO
							column_names_1st_2
						from all_tab_columns 
						where 1=1
						
						and (lower(data_type) not like '%number%' 
						or lower(column_name) like '%total_cnt%')
						
						and lower(table_name) = to_char(lower(PROCESSED_TABLE_NAME))	
						and lower(column_name) not like '%$css%'
						;


		---adding them to SELECT
		FOR K IN (
			SELECT COLUMN_VALUE AS COLUMN_NAME from TABLE(column_names_1st_2)
			) 
		LOOP
			
			final_request_columns_names_for_select_for_html := final_request_columns_names_for_select_for_html || ', ' || K.COLUMN_NAME
			;
			
		END LOOP;

		---adding other columns
		FOR K IN (
				select COLUMN_VALUE AS COLUMN_NAME from TABLE(selected_column_names)    
			)
		LOOP
			
			final_request_columns_names_for_select_for_html := final_request_columns_names_for_select_for_html || ', ' || K.COLUMN_NAME
			;
			
		END LOOP;

		---adding columns for html style attributes values 
		FOR K IN (
				select COLUMN_VALUE AS COLUMN_NAME from TABLE(selected_column_names)    
			)
		LOOP
			
			final_request_columns_names_for_select_for_html := final_request_columns_names_for_select_for_html || ', ' || K.COLUMN_NAME||'$CSS'
			;
			
		END LOOP;

		---cleaning SELECT from unnecessary space and comma symbol
		final_request_columns_names_for_select_for_html := REGEXP_REPLACE(final_request_columns_names_for_select_for_html, '(SELECT)\s*,', 'SELECT ');
			
		---assembling final request
		final_request_for_html := 
		final_request_columns_names_for_select_for_html
		|| ' FROM '
		|| PROCESSED_TABLE_NAME
		;

		---processing the case, when we don`t have any columns for SELECT-statement
		
		IF
			( CARDINALITY(selected_column_names) = 0)
		THEN
			final_request_for_html := 'empty';
		END IF;


RETURN final_request_for_html;

END RT_LOAN_BP_DATA_CHECK_KN_REQUEST_FOR_HTML_CREATE
;