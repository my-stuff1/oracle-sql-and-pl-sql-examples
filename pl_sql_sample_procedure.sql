/*
example of pl/sql procedure, written by myself - based on real procedure
*/



CREATE OR REPLACE PROCEDURE SOME_MAILOUT

/*

sending result text according to given rules

*/

  (
    PERIOD_TYPE IN CLOB DEFAULT ' ',
    MODETYPE IN CLOB DEFAULT ' ',
		EMAIL_TEXT IN CLOB DEFAULT ' '		
  )

IS

EMAIL_TITLE CLOB;
failed_emails_list_text CLOB;
successful_emails_list_text CLOB;


BEGIN

--Choosing email's subject
---processing the weird way, PL/SQL handles string comparison - via NVL
    IF (NVL (PERIOD_TYPE, ' ') = 'last_3_week_daily') THEN 
      EMAIL_TITLE := 'last 3 week daily report'
       ;
      
      ELSIF (NVL (PERIOD_TYPE, ' ') = 'last_year_monthly') THEN 
			EMAIL_TITLE := 'last year monthly report'
			 ;
			
			ELSIF (NVL (PERIOD_TYPE, ' ') = 'last_3_week_upload_date_daily') THEN 
			EMAIL_TITLE := 'last 3 week upload date daily report'
			 ;
			
		END IF;

--amending email subject based on report mode
IF (NVL(MODETYPE, ' ') != 'prod') THEN
	EMAIL_TITLE := EMAIL_TITLE || ' (TEST)';
	ELSE
		EMAIL_TITLE := EMAIL_TITLE || ' (PROD)';
END IF;


--sending emails  
IF (NVL(MODETYPE, ' ') != 'prod') 
	
	---sending if not production mode
	THEN
	
			BEGIN

					BEGIN
						 STORED_SEND_EMAIL_PROCEDURE('test_email1@email.com', EMAIL_TITLE, EMAIL_TEXT);
					END;
										
					BEGIN
						 STORED_SEND_EMAIL_PROCEDURE('test_email2@email.com', EMAIL_TITLE, EMAIL_TEXT);
					END;
			
					BEGIN
						 STORED_SEND_EMAIL_PROCEDURE('test_email3@email.com', EMAIL_TITLE, EMAIL_TEXT);
					END;
										
			END;
		
	---sending if production mode	
	ELSE
						failed_emails_list_text := null;
						successful_emails_list_text := null;
						FOR K IN (
								select EMAIL from
								employees_maillist
								WHERE REPORT_NAME = 'SOME_REPORT_NAME'
							)
						LOOP

							BEGIN
								STORED_SEND_EMAIL_PROCEDURE(K.EMAIL, EMAIL_TITLE, EMAIL_TEXT);
							
							--collecting corresponding emails if mailing error occured
							    EXCEPTION
								    WHEN OTHERS THEN
									    failed_emails_list_text := failed_emails_list_text || ' ' || K.EMAIL||' '||SQLCODE||' '||SQLERRM;
														
							END;
							
							--collecting corresponding emails if mailing was successful
							successful_emails_list_text := successful_emails_list_text || ' ' || K.EMAIL;
							
						END LOOP;
					
                    ----choosing only emails from "failed_emails_list_text" and deleting them from successful_emails_list_text (NB! doesn't work in all cases)
					successful_emails_list_text := REPLACE( successful_emails_list_text, REGEXP_SUBSTR (failed_emails_list_text,
							'[a-zA-Z0-9\.]+\@[a-zA-Z0-9]+\.[a-zA-Z0-9]+'), ' ' );

			
					--deciding whether to send mailing report
					IF failed_emails_list_text is not null THEN

							--mailing to 'my_email@company.ru' an email about those target email addresses, where mailing error occured
							
								BEGIN
									 STORED_SEND_EMAIL_PROCEDURE('my_email@company.ru', 'report mailing error '|| '"' || EMAIL_TITLE || '"', 'mailing failure:' || ' ' || failed_emails_list_text);
								END;

							--mailing to 'my_email@company.ru' an email about those target email addresses, where mailing was successful
							IF successful_emails_list_text is not null THEN
								BEGIN
									 STORED_SEND_EMAIL_PROCEDURE('my_email@company.ru', 'mailing successful '|| '"' || EMAIL_TITLE || '"', 'mailing success:' || ' ' || successful_emails_list_text);
								END;
							END IF;
							
					END IF;


END IF;

END SOME_MAILOUT;
