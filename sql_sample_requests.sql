/*
 requests written by myself, based on real ones
*/


--request 1

SELECT *
FROM 
    (
        SELECT t1.* 
        ,json_value(t1.req_data, '$.triggered_feature') AS verif_rule 
        ,t2.inn job_inn 
        ,t3.client_id_cft 
        ,count (distinct t3.client_id_cft) OVER (partition by inn) requests_from_inn 
        ,min (to_date(substr(t1.req_at,1,10),'dd.mm.yy')) OVER (partition by inn) earliest_request_from_inn 
    FROM TABLE_1 t1
    LEFT JOIN TABLE_2 t2
        ON t1.req_id = t2.req_id
    LEFT JOIN TABLE_3 t3
        ON t1.req_id = t3.req_id
    WHERE 1=1
            AND t2.inn is NOT null
            AND t3.client_id_cft is NOT null
            AND json_value(t1.req_data,'$.triggered_feature') LIKE '%"8"%'
            AND t1.req_state NOT IN ('decline_state','user_reject_state','obsolete_state')
    ORDER BY  t1.req_id DESC )
WHERE 1=1
        AND earliest_request_from_inn < to_date(sysdate-30)
        AND requests_from_inn >= 3
ORDER BY  req_id DESC
; 


--request 2

with t1 as

(select req_id, req_data, verif_rule, json_value(req_data,'$.approval_history.pledge_true.segment') as approve_true_segment, json_value(req_data,'$.approval_history.pledge_false.segment') as approve_false_segment

from test_features)

select req_id, req_data, verif_rule, approve_true_segment, approve_false_segment
from t1
where 1=1
and (approve_true_segment is  null
and approve_false_segment is not null)

and replace(json_value(req_data,'$.approval_history.pledge_true.loan_amount'),'.',',') > 1500000

order by req_id desc
;


